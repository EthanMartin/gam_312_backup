// Fill out your copyright notice in the Description page of Project Settings.


#include "Key_Item.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Ethan_Martin_GAM_312Character.h"

// Sets default values
AKey_Item::AKey_Item()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Define the scene component
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	RootComponent = SceneComponent;

	// Define the box componenet
	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	BoxComp->SetupAttachment(RootComponent);
	BoxComp->OnComponentBeginOverlap.AddDynamic(this, &AKey_Item::OnOverlapBegin);

	// Set the rotation rate
	RotationRate = FRotator(0.0f, 180.0f, 0.0f);

}

// Called when the game starts or when spawned
void AKey_Item::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AKey_Item::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Rotate the Object
	AddActorLocalRotation(RotationRate * DeltaTime);
}
		
// When the Box Componenet is overlapped, destroy the actor and print message that a key item was collected.
void AKey_Item::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Key Item Collected")));
	Destroy();
}


