// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DamageFire.generated.h"

class UBoxComponent;
class UParticleSystemComponent;

UCLASS()
class ETHAN_MARTIN_GAM_312_API ADamageFire : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADamageFire();

public:	
	// Fire Component
	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* Fire;
	// Box Component
	UPROPERTY(EditAnywhere)
		UBoxComponent* MyBoxComponent;
	// Sets the damage type as fire.
	UPROPERTY(EditAnywhere)
		TSubclassOf<UDamageType> FireDamageType;
	// The player character.
	UPROPERTY(EditAnywhere)
		AActor* MyCharacter;
	// The hit result.
	UPROPERTY(EditAnywhere)
		FHitResult MyHit;

	bool bCanApplyDamage;
	FTimerHandle FireTimerHandle;

	// Checks when overlap as started on the object.
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// Checks when overlap as ended on the object.
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	// Applies fire damage to the player.
	UFUNCTION()
		void ApplyFireDamage();

};
