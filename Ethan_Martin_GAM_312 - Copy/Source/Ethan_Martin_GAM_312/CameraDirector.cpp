// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraDirector.h"
#include "Kismet/GameplayStatics.h"
#include "Ethan_Martin_GAM_312Character.h"

// Sets default values
ACameraDirector::ACameraDirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const float TimeBetweenCameraChanges = 5.0f;
	
	TimeToNextCameraChange -= DeltaTime;

	// When the this variable reaches 0.0f, the code will check which camera is currently being displayed and switch to the other.
	/*if (TimeToNextCameraChange <= 0.0f)
	{
		TimeToNextCameraChange += TimeBetweenCameraChanges;

		APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);

		if (OurPlayerController)
		{
			// Checks if the current game view is CameraOne.
			// If it is not then the game switches to CameraOne.
			if ((OurPlayerController->GetViewTarget() != CameraOne) && (CameraOne != nullptr))
			{
				OurPlayerController->SetViewTarget(CameraOne);
			}
			// Checks if the current game view is CameraTwo.
			// If it is not then the game switches to CameraTwo.
			else if ((OurPlayerController->GetViewTarget() != CameraTwo) && (CameraTwo != nullptr))
			{
				OurPlayerController->SetViewTarget(CameraTwo);
			}
		}
	}*/
}

