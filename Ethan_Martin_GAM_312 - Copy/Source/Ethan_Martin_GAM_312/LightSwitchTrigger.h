// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LightSwitchTrigger.generated.h"

UCLASS()
class ETHAN_MARTIN_GAM_312_API ALightSwitchTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALightSwitchTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Create Point Light component
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	class UPointLightComponent* PointLight;

	// Create Light Sphere component
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	class USphereComponent* LightSphere;

	// Create Box component
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	class UBoxComponent* BoxComp;
	
	// Create Particle System component
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	class UParticleSystemComponent* Fire;
	
	// Variable for the Intenisity of the Light
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
	float LightIntensity;
	
	// Function to turn light on and off
	UFUNCTION()
	void ToggleLight();

	// Function that runs when Light Sphere is overlapped.
	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/*UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);*/
};
