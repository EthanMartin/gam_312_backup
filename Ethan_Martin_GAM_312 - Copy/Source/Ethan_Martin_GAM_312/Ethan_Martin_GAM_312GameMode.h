// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ethan_Martin_GAM_312Character.h"
#include "Ethan_Martin_GAM_312GameMode.generated.h"

class AEthan_Martin_GAM_312Character;

//Stores the current state of gameplay
UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnknown
};

UCLASS(minimalapi)
class AEthan_Martin_GAM_312GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEthan_Martin_GAM_312GameMode();
	
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	AEthan_Martin_GAM_312Character* MyCharacter;

	// Returns the current playing state 
	UFUNCTION(BlueprintPure, Category = "Health")
		EGamePlayState GetCurrentState() const;

	// Sets a new playing state 
	void SetCurrentState(EGamePlayState NewState);

	UPROPERTY(EditAnywhere, Category = "Health")
		TSubclassOf<class UUserWidget> HUDWidgetClass;

	UPROPERTY(EditAnywhere, Category = "Health")
		class UUserWidget* CurrentWidget;

private:

	// Keeps track of the current playing state 
	EGamePlayState CurrentState;



	// Handle any function calls that rely upon changing the playing state of our game 
	void HandleNewState(EGamePlayState NewState);

};



