// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Ethan_Martin_GAM_312GameMode.h"
#include "Ethan_Martin_GAM_312HUD.h"
#include "Kismet/GameplayStatics.h"
#include "Ethan_Martin_GAM_312Character.h"
#include "Blueprint/UserWidget.h"
#include "UObject/ConstructorHelpers.h"

AEthan_Martin_GAM_312GameMode::AEthan_Martin_GAM_312GameMode()
	: Super()
{

	PrimaryActorTick.bCanEverTick = true;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
	// Find the location of Health_UI Hud Widget.
	static ConstructorHelpers::FClassFinder<UUserWidget> HealthBar(TEXT("/Game/HUD/Health_UI"));
	HUDWidgetClass = HealthBar.Class;

	// Find and use our custom HUD class
	HUDClass = AEthan_Martin_GAM_312HUD::StaticClass();

	// Add Health Bar UI to viewport
	if (HUDWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);

		if (CurrentWidget)
		{
			CurrentWidget->AddToViewport();
		}

	}

}

void AEthan_Martin_GAM_312GameMode::BeginPlay()
{
	Super::BeginPlay();
	// Sets current state to playing.
	SetCurrentState(EGamePlayState::EPlaying);

	MyCharacter = Cast<AEthan_Martin_GAM_312Character>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void AEthan_Martin_GAM_312GameMode::Tick(float DeltaTime)

{

	Super::Tick(DeltaTime);
	// Checks if the player is dead and reached game over.
	if (MyCharacter)
	{
		if (FMath::IsNearlyZero(MyCharacter->GetHealth(), 0.001f))
		{
			SetCurrentState(EGamePlayState::EGameOver);
		}
	}
}


// Returns the current state.
EGamePlayState AEthan_Martin_GAM_312GameMode::GetCurrentState() const
{
	return CurrentState;
}


// Changes teh current state to the new state.
void AEthan_Martin_GAM_312GameMode::SetCurrentState(EGamePlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(CurrentState);
}


// Handles the changing between states.
void AEthan_Martin_GAM_312GameMode::HandleNewState(EGamePlayState NewState)
{
	switch (NewState)
	{
	case EGamePlayState::EPlaying:
	{
		// do nothing
	}
	break;

	// Unknown/default state
	case EGamePlayState::EGameOver:
	{
		UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
	}
	break;

	// Unknown/default state
	default:
	case EGamePlayState::EUnknown:
	{
		// do nothing
	}
	break;
	}
}
