// Fill out your copyright notice in the Description page of Project Settings.


#include "LightSwitchTrigger.h"
#include "Components/PointLightComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "DrawDebugHelpers.h"

// Sets default values
ALightSwitchTrigger::ALightSwitchTrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	// Set the value of LightIntensity
	LightIntensity = 10000.f;
	
	// Creates and defines the Point Light component.
	PointLight = CreateDefaultSubobject<UPointLightComponent>(TEXT("Point Light"));
	PointLight->Intensity = LightIntensity;
	PointLight->bVisible = false;
	RootComponent = PointLight;
	
	// Creates and defines the Box Comp component.
	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	BoxComp->BodyInstance.SetCollisionProfileName("BlockAllDynamic");
	BoxComp->SetupAttachment(RootComponent);
	
	// Creates and defines the Fire component.
	Fire = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Fire"));
	Fire->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	Fire->SetupAttachment(BoxComp);
	Fire->bVisible = false;
	
	// Creates and defines the Light Sphere component.
	LightSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Light Sphere Comp"));
	LightSphere->InitSphereRadius(300.f);
	LightSphere->SetCollisionProfileName(TEXT("Trigger"));
	LightSphere->SetupAttachment(RootComponent);
	
	// Function to check if the "Light Sphere" component has been overlapped.
	LightSphere->OnComponentBeginOverlap.AddDynamic(this, &ALightSwitchTrigger::OnOverlapBegin);
	/*LightSphere->OnComponentEndOverlap.AddDynamic(this, &ALightSwitchTrigger::OnOverlapEnd);*/

}

// Called when the game starts or when spawned
void ALightSwitchTrigger::BeginPlay()
{
	Super::BeginPlay();
	//DrawDebugSphere(GetWorld(), GetActorLocation(), 300.0f, 50, FColor::Purple, true, -1, 0, 2);
	
}

// Called every frame
void ALightSwitchTrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Turns on the light and makes the flames visible.
void ALightSwitchTrigger::ToggleLight()
{
	PointLight->ToggleVisibility();
	Fire->ToggleVisibility();
}

// Triggered when the Light Sphere component is overlapped and turns on the light.
void ALightSwitchTrigger::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Checks if the light is currently turned off.
	if (PointLight->bVisible == false && OtherActor->ActorHasTag("Player"))
	{
		// Checks if the overlapping actor is valid.
		if (OtherActor && (OtherActor != this) && OtherComp)
		{
			ToggleLight();
		}
	}
}

/*void ALightSwitchTrigger::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		ToggleLight();
	}
}*/

