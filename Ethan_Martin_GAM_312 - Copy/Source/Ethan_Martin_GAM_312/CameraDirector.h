// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraDirector.generated.h"

UCLASS()
class ETHAN_MARTIN_GAM_312_API ACameraDirector : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraDirector();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Creates property for CameraOne.
	UPROPERTY(EditAnywhere)
	AActor* CameraOne;

	// Creates property for CameraTwo.
	UPROPERTY(EditAnywhere)
	AActor* CameraTwo;

	// The time until the next camera change.
	float TimeToNextCameraChange;
};
