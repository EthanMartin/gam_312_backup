// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Key_Item.generated.h"

UCLASS()
class ETHAN_MARTIN_GAM_312_API AKey_Item : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKey_Item();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Rotation Rate
	UPROPERTY(VisibleAnywhere, Category = "Pick Up")
	FRotator RotationRate;

	// Root Component
	UPROPERTY(VisibleAnywhere, Category = "Pick Up")
	USceneComponent* SceneComponent;

	// Mesh Component
	UPROPERTY(VisibleAnywhere, Category = "Pick Up")
	class UBoxComponent* BoxComp;

	UPROPERTY(EditAnywhere)
		AActor* MyCharacter;

	// Function that will run when box component is overlapped.
	UFUNCTION()
	void  OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
