// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class Ethan_Martin_GAM_312EditorTarget : TargetRules
{
	public Ethan_Martin_GAM_312EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Ethan_Martin_GAM_312");
	}
}
